﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

// Regular Expressions
// 
// \[([\s\S]*?)\] 
//      Capture everything between [ .... ]
// 
// \"(.*?)\"
//      Capture everyhthing between quotation marks
//
// \"([\s\S]*?)\"
//      Capture everything between quotation marks (MULTILINE)
//      ^ use this for capturing the body of a diary record.
// 
// <(.*?)>
//      Strip anything inside of <> e.g HTML
//
// Test these at regexr.com

namespace DarksideRulesParser
{
    /// <summary>
    /// Author: Lynxaa
    /// This program is designed to work with The Darkside's mission file, but it may work with others, who knows.
    /// </summary>
    internal class Program
    {
        // fk resharpers name suggestions for local/non-local variables, pft.
        private static readonly Dictionary<string, string> pairs = new Dictionary<string, string>();
        private const string _DIRECTORY = "gen";
        private static string _STYLE_TIMESTAMP = "1";
        private static readonly Random _RANDOM = new Random();
        private static readonly object _SYNCLOCK = new object();
        private static readonly string TIME = DateTime.Now.ToString("dd/MM/yy H:mm:ss tt");

        #region functions
        /// <summary>
        /// Checks to see if the file "styles.css" exists, if it does we'll set <seealso cref="_STYLE_TIMESTAMP"/> to a 
        /// generated version ID based off the timestamp shifted 16 bits forwards
        /// </summary>
        private static void CheckStyling() {
            string style = Path.Combine(Directory.GetCurrentDirectory(), "style.css");
            if (!File.Exists(style)) {
                return;
            }

            _STYLE_TIMESTAMP = $"{Math.Abs(File.GetLastWriteTime(style).ToString("yyyyMMddTHHmmss").GetHashCode() >> 16)}";
        }

        /// <summary>
        /// Generates a random string, this function will always return something new even inside of loops and multiple threads.
        /// </summary>
        /// <param name="length">length</param>
        /// <returns></returns>
        private static string GetRandomString(int length) {
            const string chars = "abcdefghijklmnopqrswxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            lock (_SYNCLOCK) {
                return new string(Enumerable.Repeat(chars, length).Select(s => s[_RANDOM.Next(s.Length)]).ToArray());
            }
        }

        /// <summary>
        /// A function to escape a string
        /// </summary>
        /// <param name="str">a string to escape special characters from (html tags & quotes)</param>
        /// <returns></returns>
        private static string EscapeString(string str)  {
            str = Regex.Replace(str, @"\""", "");
            str = Regex.Replace(str, @"<(.*?)>", "");
            return str;
        }

        /// <summary>
        /// A function to escape a string
        /// </summary>
        /// <param name="str">a string to escape special characters from (html tags, quotes AND double spaces)</param>
        /// <returns></returns>
        private static string EscapeString2(string str) {
            str = Regex.Replace(str, @"\""", "");
            str = Regex.Replace(str, @"<(.*?)>", "");
            str = Regex.Replace(str, @"\s\s+", "");
            return str;
        }

        private static string CreatePanel(string title, bool addBackNav = false, bool isParent = false) {
            string c = isParent ? "panel-main" : "";
            string back = addBackNav ? $@"<span class='pull-right'><strong><a href='../'>Go back...</a></strong></span>" : "";
            return $@"<div class=""panel panel-default {c}""><div class=""panel-heading"">{title}{back}</div>";
        }

        private static string StartPanelBody(bool isParent = false) {
            string c = isParent ? "panel-main-body" : "";
            return $@"<div class=""panel-body {c}"">";
        }

        private static string FinishPanelBody() {
            return "</div>";
        }

        private static string CreatePanelBody(string content) {
            return $@"<div class=""panel-body"">{content}</div>";
        }

        private static string FinishPanel() {
            return $@"
<div class=""panel-footer"">
	<span class=""pull-right""><strong><a href=""http://thedarksidegames.invisionzone.com/"" target=""_blank"">thedarksidegames.invisionzone.com</a></strong></span>
  	<span>This file was generated at {TIME}</span>
</div>";
        }

        /// <summary>
        /// Waits for the user to press a key before continuing.
        /// </summary>
        private static void WaitForInput() {
            Console.WriteLine();
            Console.WriteLine("Press any key to continue....");
            Console.ReadKey();
        }

        /// <summary>
        /// Checks to see if a folder (<seealso cref="_DIRECTORY"/>) exists, if not it'll create it.
        /// </summary>
        /// <returns>A string containing the path to our folder</returns>
        private static string GetPath() {
            if (!Directory.Exists(_DIRECTORY)) {
                Directory.CreateDirectory(_DIRECTORY);
            }

            return Path.Combine(Directory.GetCurrentDirectory(), _DIRECTORY);
        }

        /// <summary>
        /// Checks to see if a folder (<seealso cref="_DIRECTORY"/>) exists, if it does it'll remove all contents from it
        /// on a basic level (no recursion) and then remove index.html
        /// </summary>
        private static void DeleteExistingItems() {
            if (Directory.Exists(GetPath())) {
                foreach (string file in Directory.GetFiles(GetPath())) {
                    Console.WriteLine($"Deleting existing file: {file}");
                    File.Delete(file);
                }

                Console.WriteLine($"Deleting folder: {GetPath()}");
                Directory.Delete(GetPath());
            }

            string root = Path.Combine(Directory.GetCurrentDirectory(), "index.html");
            if (File.Exists(root))
                File.Delete(root);
        }

        /// <summary>
        /// Loops over our pair array (<seealso cref="pairs"/>) and compare the key against our passed key
        /// </summary>
        /// <param name="key">The key to compare against</param>
        /// <returns></returns>
        private static string HasUniqueName(string key) {
            string uniqueName = key;
            foreach (KeyValuePair<string, string> pair in pairs) {
                if (pair.Key.Equals(key, StringComparison.CurrentCultureIgnoreCase)) {
                    uniqueName = pair.Value;
                    break;
                }
            }

            return uniqueName;
        }

        /// <summary>
        /// Creates a file
        /// </summary>
        /// <param name="filename">The name of a file to create</param>
        /// <param name="isIndex">If this is passed as true, we'll avoid calling HasUniqueName and set the uniqueName to "Home"</param>
        private static void CreateFile(string filename, bool isIndex = false) {
            string uniqueName = isIndex ? "Home" : HasUniqueName(filename);
            string back = isIndex ? "" : "<a href=\"../\">Go Back...</a>";
            string style = isIndex ? "style.css" : "../style.css";

            string path = Path.Combine(GetPath(), $"{filename}.html");
            if (!File.Exists(path)) {
                File.Create(path).Close();
                string head = $@"
<head>
    <title>{uniqueName}</title>
    <meta charset=""utf-8"">
    <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1"">
    <script src=""https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js""></script>
    <link rel=""stylesheet"" href=""https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"" integrity=""sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"" crossorigin=""anonymous"">
    <script src=""https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"" integrity=""sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"" crossorigin=""anonymous""></script>
    <link rel=""stylesheet"" href=""{style}?v={_STYLE_TIMESTAMP}"">
</head>";
                File.AppendAllText(path, head);

                string body = $@"<body class='container'>{CreatePanel(uniqueName, !isIndex, true)}{StartPanelBody(true)}";
                File.AppendAllText(path, body);
            }
        }

        /// <summary>
        /// Appends the filename param to the result of <seealso cref="GetPath()"/>, and then calls <seealso cref="File.AppendAllText"/>
        /// </summary>
        /// <param name="filename">The name of the file to append text to</param>
        /// <param name="text">The text we want to append onto the end of the files contents</param>
        private static void AppendToFile(string filename, string text) {
            string path = Path.Combine(GetPath(), $"{filename}.html");
            File.AppendAllText(path, text);
        }

        /// <summary>
        /// This function expects our match param to have a count of 3.
        /// match[0] will be the filename
        /// match[1] will be the header text
        /// match[2] will be the contents, which will be split into lines and have a break-line tag appended to the end of them
        /// </summary>
        /// <param name="match">The result of <seealso cref="Regex.Matches"/></param>
        private static void GeneratedFiles(MatchCollection match) {
            string filename = EscapeString(match[0].ToString());
            CreateFile(filename);

            string id = GetRandomString(16);

            string headerText = EscapeString(match[1].ToString());
            if (headerText.Length == 0)
                headerText = HasUniqueName(filename);

            // generate the body text
            string body = EscapeString(match[2].ToString());
            string[] bodyLines = body.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            body = $@"<div class='panel panel-default panel-nested'><div class='panel-heading' onclick='toggle(""#{id}"")'>{headerText}</div><div class='panel-body' id=""{id}"" style='display:none;'>";

            for (int i = 0; i < bodyLines.Length; i++) {
                string br = "<br/>";
                if (((i + 1) == bodyLines.Length) || i == 0)
                    br = "";
                string line = bodyLines[i];
                if (string.IsNullOrEmpty(line) || ((i + 1) == bodyLines.Length && bodyLines.Length > 2))
                    continue;
                body += $"<span>{EscapeString2(line)}</span>{br}{Environment.NewLine}";
            }

            body += "</div></div>";
            AppendToFile(filename, body);
        }

        /// <summary>
        /// This function loops over the content of our folder (defined <seealso cref="_DIRECTORY"/>) and gets each file inside of it, then wraps the file name in an anchor tag with a link to it.
        /// <seealso cref="CreateFile"/>
        /// </summary>
        private static void GenerateIndexFile() {
            string content = "";

            // somehow this is how you order alphabetically in C#, comparsion magic I call it.
            foreach (string filename in Directory.EnumerateFiles(GetPath()).OrderBy(f => f)) {
                string file = Path.GetFileNameWithoutExtension(filename);
                string link = $@"{_DIRECTORY}\{file}.html";
                content += $@"<p><a href=""{link}"">{HasUniqueName(file)}</a></p>";
            }

            content += FinishPanelBody() + FinishPanel();

            string path = Path.Combine(Directory.GetCurrentDirectory(), "index");
            CreateFile(path, true /* don't try find a key value for the title attr */);
            File.AppendAllText($"{path}.html", content);
        }

        /// <summary>
        /// Since our HTML is incomplete due to the fact we constantly append data without any proper logic behind, this function will append the remaining data required, e.g the body tags and other tags needed to be closed
        /// </summary>
        private static void Cleanup() {
            if (Directory.Exists(GetPath())) {
                foreach (string file in Directory.GetFiles(GetPath())) {
                    Console.WriteLine($"Cleaning up data in file: {file}");
                    File.AppendAllText(file, $"{FinishPanelBody() + FinishPanel()}</body>");
                    File.AppendAllText(file, @"<script type='text/javascript'>var toggle=function(a){var b=$(a);b.slideToggle(""fast"")};</script>");
                    File.AppendAllText(file, @"<script type='text/javascript'>var resizeMainBody=function(){var a=$("".panel-main-body""),b=$(window).height()-120;a.css(""max-height"",b+""px"")};$(window).on(""resize"",function(a){resizeMainBody()}),$(window).on(""load"",function(a){resizeMainBody()});</script>");
                }
            }

            string root = Path.Combine(Directory.GetCurrentDirectory(), "index.html");
            if (File.Exists(root)) {
                Console.WriteLine($"Cleaning up data in file: {root}");
                File.AppendAllText(root, @"<script type='text/javascript'>var resizeMainBody=function(){var a=$("".panel-main-body""),b=$(window).height()-120;a.css(""max-height"",b+""px"")};$(window).on(""resize"",function(a){resizeMainBody()}),$(window).on(""load"",function(a){resizeMainBody()});</script>");
                File.AppendAllText(root, $"</body>");
            }
        }
        #endregion

        static void Main(string[] args) {
            Console.WriteLine("Created by Lynxaa");
            Console.WriteLine("This program will parse everything inside of breifing.sqf and make it into HTML");
            Console.WriteLine("-------------------------------------------------------------------------------");
            
            if (args.Length <= 0) {
                Console.WriteLine("Drag the file you want to parse onto this executable in order for it to work*");
                WaitForInput();
                return;
            }

            DeleteExistingItems();
            CheckStyling();

            string contents = File.ReadAllText(args[0]);
            contents = Regex.Replace(contents, @"-{2,}", "");

            foreach (Match m in Regex.Matches(contents, @"\[([\s\S]*?)\]", RegexOptions.Compiled)) {
                MatchCollection match = Regex.Matches(m.Value, @"\""([\s\S]*?)\""", RegexOptions.Compiled);

                if (match.Count == 2) {
                    try {
                        pairs.Add(EscapeString(match[0].ToString()), EscapeString(match[1].ToString()));
                    } catch (Exception e) {
                        Console.WriteLine($"ERROR: {e.Message}");
                    }
                } else if (match.Count == 3) {
                    string filename = EscapeString(match[0].ToString());
                    GeneratedFiles(match);
                    Console.WriteLine($"Writing parsed data into {_DIRECTORY}/{filename}.html");
                }
            }

            GenerateIndexFile();
            Cleanup();
            Console.WriteLine("Done!");

            WaitForInput();
        }
    }
}
